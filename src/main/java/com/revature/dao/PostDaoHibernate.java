package com.revature.dao;

import java.util.List;
import com.revature.model.Post;


public interface PostDaoHibernate {
	
	//Used to show all posts on the feed
	//Investigate limited call to only get ten at a time
	//Dont want to transfer all posts across the network at one time
	public List<Post> selectAllPosts();
	
	//Used to view certain users posts
	public List<Post> selectPostByUsername();
	
	//Create a new post
	public void insertPost(Post newPost);
	
	//Like a post, add one to post's like count
	public void incrementLikePostCount(Post likedPost);
	
	//Unlike a post, subtract one from a post's like count
	public void deccrementLikePostCount(Post unlikePost);
}
