package com.revature.dao;

import com.revature.model.User;

public interface UserDaoHibernate {

	//Checks if username give is unique
	//Used for creating new user
	public boolean isUniqueUsername(String username);


	//Send in user with just username and password
	//Used to get full user info after logging in
	//Also used to view others personal information
	public User selectUser(User user);


	//Used to create a new user
	public void insertUser(User newUser);


	//Selectively updates user
	//Make sure to test different attributes
	public void updateUser(User userToUpdate);
}
